using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PArtículas : MonoBehaviour
{
    public ParticleSystem Particulas;
    public int mass;
    
    //public Color nieve (2.08f, 2.55f, 2.55f);


    // Start is called before the first frame update
    void Start()
    {

       

    }

    // Update is called once per frame
    void Update()
    {


       
        

    }

    public void Encender()
    {

        var emission = Particulas.emission;
        emission.enabled = true;


    }

    public void Apagar()
    {

        var emission = Particulas.emission;
        emission.enabled = false;


    }

    public void Mas()
    {

        var emission = Particulas.emission;
        mass += 20;
        emission.rateOverTime = mass;



    }

    public void Menos()
    {

        var emission = Particulas.emission;
        mass -= 20;
        emission.rateOverTime = mass;



    }


    public void azul()
    {

        var main = Particulas.main;
        //Color newColor = new Color(0.3f, 0.4f, 0.6f);
        main.startColor = Color.blue;


    }


    public void rojo()
    {

        var main = Particulas.main;

        main.startColor = Color.red;


    }

    public void verde()
    {

        var main = Particulas.main;

        main.startColor = Color.green;


    }




}
